using NUnit.Framework;
using Array_List;
using System;

namespace Array_List.Tests
{
    public class Tests
    {
        [Test]
        [TestCase(1,2,3,4,5,6)]
        [TestCase(0)]
        public void Add_Elements_ReturnCount(params int[] elements)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(elements);

            //act
            var expected = elements.Length;
            var actual = list.Count;

            //assert
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(elements[0], list[0]);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(2)]
        public void Get_WrongIndex_ThrowsIndexOutOfRange(int index)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1);

            //act
            Assert.Throws<IndexOutOfRangeException>(() => list[index].ToString());
        }

        [Test]
        public void Clear_ReturnCount0()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3);

            //act
            list.Clear();
            int count = list.Count;

            //arrange
            Assert.AreEqual(0, count);
        }        
        
        [Test]
        [TestCase(3, new int[] { 1, 2, 3, 4, 5 }, true)]
        [TestCase(100, new int[] { 1, 2, 3, 4, 5 }, false)]
        [TestCase(0, new int[] {}, false)]
        public void Contains_TrueOrFalseReturned(int elementsToFind, int [] elements, bool expected)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(elements);

            //act
            var isContains = list.Contains(elementsToFind);
            
            //arrange
            Assert.AreEqual(expected, isContains);
        }


        [Test]
        [TestCase(2, new int[] { 1, 2, 3, 4, 2, 6 }, 1)]
        [TestCase(-10, new int[] { 1, 2, 3, 4, 5, 6 }, -1)]
        [TestCase(0, new int[] {}, -1)]
        public void  IndexOf_Element_ExpectedPositionReturned(int element, int[] elements, int expected)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(elements);

            //act
            var position = list.IndexOf(element);

            //arrange
            Assert.AreEqual(expected, position);
        }

        [Test]
        [TestCase(2, new int[] { 1, 2, 3, 4, 5 }, 4)]
        [TestCase(10, new int[] { 1, 2, 3, 4, 5 }, 5)]
        [TestCase(-1, new int[] { -1, 2, -1, 4, 5 }, 4)]
        [TestCase(-1, new int[] { -1, 2, -1, 4, 5 }, 4)]
        [TestCase(100, new int[] { -1, 2, -1, 4, 100}, 4)]
        [TestCase(1, new int[] {1}, 0)]
        public void Remove_Element_ReturnedExpectedCount(int elementToRemove, int [] elements, int expectedCount)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(elements);

            //act
            list.Remove(elementToRemove);

            //arrange
            Assert.AreEqual(expectedCount, list.Count);        
        }

        public void Remove_ThrowArgumentNullException()
        {
            //arrange
            CustomList<object> list = new CustomList<object>(1,2,3);

            //arrange
            Assert.Throws<ArgumentNullException>(() => list.Remove(null));

        }

        [Test]
        [TestCase(2, new int[] { 1, 2, 3, 4, 5 }, 4)]
        [TestCase(0, new int[] { 1, 2, 3, }, 2)]
        [TestCase(0, new int[] { 1 }, 0)]
        public void RemoveAt_Position_ExpectedCountReturned(int position, int[] elements, int expectedCount)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(elements);

            //act
            list.RemoveAt(position);

            //arrange
            Assert.AreEqual(expectedCount, list.Count);
        }

        [Test]
        [TestCase(-1)]
        [TestCase(3)]
        public void RemoveAt_WrongPosition_ThrowIndexOutOfRangeException(int position)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 0);

            Assert.Throws<ArgumentOutOfRangeException>(() => list.RemoveAt(position));
        }

        [Test]
        [TestCase(0)]
        [TestCase(5)]
        [TestCase(2)]
        public void Insert_AtPositionValue_ReturnCount(int position)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 7, 8, 10);

            //act
            int elementToInsert = 100;
            list.Insert(position, elementToInsert);
            int count = list.Count;

            //arrange
            Assert.AreEqual(6, count);
        }

        [Test]
        [TestCase(-2)]
        [TestCase(6)]
        public void Insert_AtWrongPositionValue_ThrowArgumentOutOfRange(int position)
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3, 4, 5);

            //act
            Assert.Throws<ArgumentOutOfRangeException>(() => list.Insert(position, 100));
        }

        [Test]
        public void Insert_AtPositionNullValue_ThrowsArgumentsNullException()
        {
            //arrange
            CustomList<object> list = new CustomList<object>();

            //assert
            Assert.Throws<ArgumentNullException>(() => list.Insert(0, null));

        }

        [Test]
        public void CopyTo_ReturnsArray()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3, 4, 5);
            int[] array = new int[10];

            //act
            list.CopyTo(array, 2);

            //assert
            Assert.AreEqual(array[2], 1);
        }

        [Test]
        public void CopyTo_ArrayIsNull_ThrowsArgumentNullException()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3, 4, 5);
            int[] array = null;

            //assert
            Assert.Throws<ArgumentNullException>(() => list.CopyTo(array, 2));
        }

        [Test]
        public void CopyTo_ThrowsArgumentException()
        {
            //arrange
            CustomList<int> list = new CustomList<int>(1, 2, 3, 4, 5);
            int[] array = new int[2];

            //assert
            Assert.Throws<ArgumentException>(() => list.CopyTo(array, 2));
        }
    }
}